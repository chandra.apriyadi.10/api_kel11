/*
SQLyog Ultimate v13.1.1 (64 bit)
MySQL - 10.4.27-MariaDB : Database - db_kel_11
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_kel_11` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */;

USE `db_kel_11`;

/*Table structure for table `aset` */

DROP TABLE IF EXISTS `aset`;

CREATE TABLE `aset` (
  `id_aset` int(4) NOT NULL AUTO_INCREMENT,
  `nama_aset` varchar(50) DEFAULT NULL,
  `jumlah_aset` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_aset`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `aset` */

insert  into `aset`(`id_aset`,`nama_aset`,`jumlah_aset`) values 
(1,'Ruang 201B','1'),
(2,'Ruang 202B','1'),
(3,'Ruang 203B','1'),
(4,'Mic Wireless','2'),
(5,'Kotak Tisu','14'),
(6,'Kabel Jack','5'),
(7,'Stand Mic','8'),
(9,'Vas Bunga','10'),
(10,'Taplak Meja','15');

/*Table structure for table `peminjaman` */

DROP TABLE IF EXISTS `peminjaman`;

CREATE TABLE `peminjaman` (
  `id_peminjaman` int(4) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `id_aset` int(4) NOT NULL,
  `nama_aset` varchar(50) NOT NULL,
  `tanggal_pinjam` date NOT NULL,
  `persetujuan` set('Di Setujui','Tidak di Setujui') DEFAULT NULL,
  PRIMARY KEY (`id_peminjaman`),
  KEY `id_user` (`id_user`),
  KEY `id_aset` (`id_aset`),
  CONSTRAINT `peminjaman_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`),
  CONSTRAINT `peminjaman_ibfk_2` FOREIGN KEY (`id_aset`) REFERENCES `aset` (`id_aset`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `peminjaman` */

insert  into `peminjaman`(`id_peminjaman`,`id_user`,`name`,`id_aset`,`nama_aset`,`tanggal_pinjam`,`persetujuan`) values 
(1,20311077,'Chandra Apriyadi',3,'Ruang 203B','2023-06-18','Di Setujui'),
(2,0,'Chandra Apriyadi',3,'Ruang 203B','2023-06-18','Di Setujui'),
(77,20301111,'Siapa Ya',9,'Vas Bunga','2023-06-19','Di Setujui');

/*Table structure for table `pengembalian` */

DROP TABLE IF EXISTS `pengembalian`;

CREATE TABLE `pengembalian` (
  `id_pengembalian` int(4) NOT NULL AUTO_INCREMENT,
  `id_peminjaman` int(4) NOT NULL,
  `id_user` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `tanggal_kembali` date NOT NULL,
  `sudah_dikembalikan` set('Sudah','Belum') DEFAULT NULL,
  PRIMARY KEY (`id_pengembalian`),
  KEY `id_peminjaman` (`id_peminjaman`),
  KEY `id_user` (`id_user`),
  CONSTRAINT `pengembalian_ibfk_2` FOREIGN KEY (`id_peminjaman`) REFERENCES `peminjaman` (`id_peminjaman`),
  CONSTRAINT `pengembalian_ibfk_3` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `pengembalian` */

insert  into `pengembalian`(`id_pengembalian`,`id_peminjaman`,`id_user`,`name`,`tanggal_kembali`,`sudah_dikembalikan`) values 
(1,1,20311077,'Chandra','2023-06-19','Sudah'),
(13,2,20311077,'Chandra Apriyadi','2023-06-18','Sudah'),
(14,1,20311077,'Chandra Apriyadi','2023-06-18','Belum'),
(15,1,20311077,'Chandra Apriyadi','2023-06-18','Belum'),
(34,2,20311077,'Dia','2023-06-19','Belum'),
(43,2,2,'2','0000-00-00','Sudah'),
(48,2,2,'10','0000-00-00','Sudah');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `role` set('Admin','Mahasiswa','Dosen','Karyawan') NOT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `prodi` set('S1 Sistem Informasi','S1 Informatika','D3 Sistem Informasi Akuntansi','S1 Teknik Sipil','S1 Teknik Komputer','S1 Teknik Elektro','S1 Teknologi Informasi','S1 Akuntansi','S1 Manajemen','S1 Sastra Inggris','S1 Pendidikan Bahasa Inggris','S1 Pendidikan Matematika','S1 Olahraga') NOT NULL,
  `hp` varchar(15) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=20311095 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `user` */

insert  into `user`(`id_user`,`name`,`username`,`pass`,`role`,`alamat`,`prodi`,`hp`) values 
(0,'admin','tes','28b662d883b6d76fd96e4ddc5e9ba780','Admin',NULL,'','085273141599'),
(2,'admin2','Admin2','7a8a80e50f6ff558f552079cefe2715d','Admin',NULL,'','085273141571'),
(4,'tess1','tes11','22daf1a39b6e5ea16554f59e472d96f6','Admin',NULL,'','085273141573'),
(5,'coba','coba','c3ec0f7b054e729c5a716c8125839829','Admin',NULL,'','085273141575'),
(20301111,'Siapa Ya','Siapa','2128e15b849bb3d5b1fa88cc18d494fe','Dosen',NULL,'','085432199999'),
(20311077,'Chandra Apriyadi','Chandra','de03c6de427184afe57262de14d084d7','Mahasiswa','Harapan Indah yang Tak Kunjung Terwujud','S1 Sistem Informasi','085273141570'),
(20311078,'Dimas Wahyu','Dimas','8555f45b58e4806a1f68627339644129','Mahasiswa','Jalan Satu-satunya yang Hanya Dapat dilalui','S1 Sistem Informasi','085273141579'),
(20311079,'Muhammad Romdoni','Romdoni','718ea5dd8c4054b0a48198b23417f4d2','Mahasiswa','Gak ada rumah','S1 Sistem Informasi','085273141566'),
(20311080,'Urip Hadi','Urip','20f3c3909412815e1a034aa6c54541b2','Mahasiswa','Jalan Hayalan yang Tak Tau Dapat ditemui atau Tidak','S1 Teknik Sipil','085273141511'),
(20311081,'Dia Siapa','Dia','465b1f70b50166b6d05397fca8d600b0','Karyawan',NULL,'','082134678654'),
(20311086,'akuuuu','aku','89ccfac87d8d06db06bf3211cb2d69ed','Mahasiswa','ada','S1 Sistem Informasi','08654321234'),
(20311088,'diaa','diaa','465b1f70b50166b6d05397fca8d600b0','Mahasiswa','dia','S1 Sistem Informasi','09876543245');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
